const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    app: [
      'webpack-dev-server/client?http://0.0.0.0:9091',
      'webpack/hot/only-dev-server',
      './src/main.js',
    ],
  },
  output: {
    path: 'build',
    filename: '[name].bundle.js',
    publicPath: '/',
  },
  resolveLoader: {
    root: path.join(__dirname, 'node_modules'),
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: `"${process.env.NODE_ENV}"`,
      },
    }),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      chunks: ['app'],
      template: 'src/html/index.html',
      filename: 'index.html',
    }),
  ],
  module: {
    loaders: [
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff',
      },
      {
        test: /\.(ttf|eot|svg|ico|jpg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader?name=[name].[ext]',
      },
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loaders: ['react-hot-loader/webpack', 'babel-loader'],
      },
      {
        test: /\.(scss|css)$/,
        loader: 'style-loader!css-loader!sass-loader',
      },
    ],
  },
  devServer: {
    hot: true,
    noInfo: false,
    historyApiFallback: true,
    publicPath: '/',
    stats: 'errors-only',
    port: 9091,
  },
  sassLoader: {
    includePaths: [path.resolve(__dirname, './node_modules/font-awesome')],
  },
};
