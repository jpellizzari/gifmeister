# Gifmeister

View trending gifs from Giphy and save them to your favorites

### Installation

Install dependencies with `npm install`

Start the server with `npm start`

Visit http://localhost:9091/ to see it in action!

Also works with `yarn`: `yarn --pure-lockfile && yarn start`

## Notes

Some thoughts and considerations I had while running through this exercise:

### Design

I took some time to think about "branding" my Giphy client. Gifs are generally silly and fun, so I chose a couple of colors that were loud and distinct to match the "fun" feeling we want to convey to users. I also came up with a basic logo that would hopefully be more memorable than just plain text.

Once I had a very basic aesthetic defined, I came up with 4 baseline features I wanted to implement:

1. **Mosaic layout**: since gifs would be all different sizes, a mosaic makes sense. Also, the trending gifs aren't in any type of order (since they are curated by humans)

2. **Infinite scroll**: since Gifs are largely visual, users will want to browse heavily. Instead of a clunky pagination interaction, lets just load more when they get to the bottom of the page

3. **Favorites**: Users should be able to save their favorite gifs for later use. Keeping a curated list of reaction gifs is crucial to effective communication in modern business.

4. **Sharing**: It would be cool if users were able to quickly share gifs via Gifmeister. The whole point of these gifs is to send to others, so we should make that easy.

I created a basic concept in Sketch to guide the implementation, as well as a quick branding/style-guide to act as the source of truth for colors and the logo.

I also took into consideration that Gifmeister will need to look good on mobile, tablet, and desktop so I chose a layout and navigation pattern that I new would translate well to each of those platforms (with a minimal amount of CSS media queries).

The Sketch file is included in this repo in the `/design` directory.

### Implementation

#### The stack

I went with tried and true tools that I have used before. On green-field projects like this, I have to resist the urge to use new tech to avoid sinking time into reading tutorials and troubleshooting.

I used React/Redux for view and state logic, respectively, coupled with `redux-thunk` for async state changes. I also used an immutable data structure library called `icepick`, which utilizes frozen objects. The main advantage of `icepick` are that the consumers of our state data can just user regular JavaScript functions on the data.

Libraries like Immutable.js introduces opaque types that do not work with destructuring and make the code much more verbose. Also, any attempt to mutate frozen data will throw a runtime error (Immutable.js fails silently).

For styling, I used `styled-components` with a centralized theme to make it easy to change values globally.

#### Challenges

Here are some notes about the challenges of implementing my design. Given more time these are the primary areas I would like to improve:

##### Mosaic

I quickly found some tutorials on how to do a column layout with images of varying heights using the `column-count` property. This approach made it easy to manipulate the layout for mobile with media queries. This approach works well for the initial page, but falls short when new gifs are loaded. The items cascade vertically, so new gifs result in existing gifs being placed into separate columns.

It appears that the actual Giphy trending page uses absolute positioning via a more complex layout engine to get around this issue.

##### Repaints!

My initial implementation loaded 25 gifs. Very soon after page load, the UI and even my dev tools became unresponsive to any type of input. Loading that many gifs seemed to require constant repaints, which didn't leave any time for click handlers to run. I tried several different approaches, including `requestAnimationFrame` trickery, but none worked. I ended up using the downsampled gif URLs, which is a crappy workaround from the user's perspective.

##### Sharing

I dropped this feature entirely for time constraints. Figuring out how to get around the repainting issue ate up more time that expected.

#### Tooling/testing

I borrowed a working `webpack` v1 config file from an existing project in an effort to avoid spending any time in lost in the `webpack` dependency jungle. I also borrowed a working `eslint`, `prettier`, and `babel` config file.

For testing, I decided only to test state-change business logic. I generally prefer to write tests for my React components, but for this project, they were very basic and unit tests would not have been worth the time.
