import React from 'react';
import { BrowserRouter as Router, Route, Redirect, NavLink } from 'react-router-dom';
import styled, { injectGlobal } from 'styled-components';
import { connect } from 'react-redux';

import { loadNextTrendingPage } from './actions';

import Logo from './components/logo';

import TrendingPage from './pages/trending';
import FavoritesPage from './pages/favorites';

(() => injectGlobal`
  html {
    height: 100%;
    overflow: hidden;
  }

  body {
    font-family: 'Helvetica', sans-serif;
    color: black;
    margin: 0;
  }

  ul, ol {
    list-style: none;
    margin: 0;
    padding: 0;
  }
`)();

const NavBar = styled.nav`
  display: flex;
  padding: 0 16px;
  margin: 8px 0;
  justify-content: space-between;
  height: 32px;
  line-height: 32px;
`;

const NavButtons = styled.div`
  align-self: right;
`;

const LogoWrapper = styled.div`
  position: relative;
`;

const StyledLink = styled(NavLink)`
  border-radius: 3px;
  padding: 6px 12px;
  text-decoration: none;

  &:last-child {
    margin-left: 8px;
  }

  /* Provided by react-router */
  &.active {
    background-color: ${props => props.theme.colors.primary};
    color: ${props => props.theme.colors.white};
  }
`;

const Styled = component => styled(component)`
  max-height: 100vh;
  overflow: auto;
  position: relative;
  max-width: 1024px;
  margin: 0 auto;
`;

class App extends React.PureComponent {
  componentDidMount() {
    this.tryLoadMore();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.count !== this.props.count) {
      this.tryLoadMore();
    }
  }

  tryLoadMore = () => {
    // Only try to auto-load if we are on the trnding page.
    const isTrendingPage = !!(window.location.pathname === '/trending');
    // Quick and dirty scroll detector
    const isBottom = !!(
      this.viewPort.scrollHeight - this.viewPort.scrollTop ===
      this.viewPort.clientHeight
    );
    if (isTrendingPage && isBottom) {
      this.props.loadNextTrendingPage();
    }
  };
  render() {
    return (
      <div
        className={this.props.className}
        ref={(element) => {
          this.viewPort = element;
        }}
        onScroll={this.tryLoadMore}
      >
        <Router>
          <React.Fragment>
            <NavBar>
              <LogoWrapper>
                <NavLink to="/">
                  <Logo />
                </NavLink>
              </LogoWrapper>
              <NavButtons>
                <StyledLink to="/trending">Trending</StyledLink>
                <StyledLink to="/favorites">Favorites</StyledLink>
              </NavButtons>
            </NavBar>
            <Route exact path="/" render={() => <Redirect to="/trending" />} />
            <Route path="/trending" component={TrendingPage} />
            <Route path="/favorites" component={FavoritesPage} />
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = ({ gifs }) => ({
  count: gifs.length,
});

export default connect(mapStateToProps, { loadNextTrendingPage })(Styled(App));
