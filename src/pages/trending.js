import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { map } from 'lodash';

import { getTrending, toggleFavorite } from '../actions';

import Page from '../components/page';
import Mosaic from '../components/mosaic';
import Image from '../components/image';

const Styled = component => styled(component)``;

class Trending extends React.PureComponent {
  componentDidMount() {
    this.props.getTrending();
  }

  handleFavoriteClick = (image) => {
    this.props.toggleFavorite(image);
  };

  render() {
    const { className, gifs, favorites } = this.props;
    return (
      <div className={className}>
        <Page>
          <Mosaic>
            {map(gifs, i => (
              <Image
                key={i.id}
                onClick={this.handleFavoriteClick}
                image={i}
                isFavorite={!!favorites[i.id]}
              />
            ))}
          </Mosaic>
        </Page>
      </div>
    );
  }
}

const mapStateToProps = ({ gifs, favorites }) => ({ gifs, favorites });

export default connect(mapStateToProps, { getTrending, toggleFavorite })(Styled(Trending));
