import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { map, values, sortBy } from 'lodash';
import { Link } from 'react-router-dom';

import { toggleFavorite } from '../actions';

import Page from '../components/page';
import Mosaic from '../components/mosaic';
import Image from '../components/image';

const Styled = component => styled(component)`
  p {
    text-align: center;
  }
`;

class Favorites extends React.PureComponent {
  handleFavoriteClick = (image) => {
    /* eslint-disable no-alert */
    const result = window.confirm('Are you sure you want to remove this gif from your favorites?');

    if (result) {
      this.props.toggleFavorite(image);
    }
  };
  render() {
    const { className, favorites } = this.props;
    // Sort to ensure that favs always come up in the same order.
    const images = sortBy(values(favorites), 'id');

    return (
      <div className={className}>
        <Page>
          {images && images.length === 0 ? (
            <p>
              No favorites selected! Visit the <Link to="/trending">trending</Link> page to select
              your favorite gifs.
            </p>
          ) : (
            <Mosaic>
              {map(images, i => (
                <Image
                  key={i.id}
                  onClick={this.handleFavoriteClick}
                  image={i}
                  isFavorite={!!favorites[i.id]}
                />
              ))}
            </Mosaic>
          )}
        </Page>
      </div>
    );
  }
}

const mapStateToProps = ({ favorites }) => ({ favorites });

export default connect(mapStateToProps, { toggleFavorite })(Styled(Favorites));
