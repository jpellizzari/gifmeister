/* eslint-disable global-require */
import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from 'styled-components';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { freeze, merge } from 'icepick';

import theme from './theme';
import reducer, { initialState } from './reducer';
import { createStorageEngine } from './utils';
import { LOCAL_STORAGE_KEY } from './constants';

const devTools =
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const storage = createStorageEngine(LOCAL_STORAGE_KEY);
const prevState = storage.get('APP_STATE');

// Initialize the redux store with the previous state from localStorage
const store = createStore(
  reducer,
  freeze(merge(initialState, prevState)),
  compose(applyMiddleware(thunk), devTools)
);

// Keep localStorage in sync with redux state
store.subscribe(() => {
  storage.set('APP_STATE', store.getState());
});

// Hot-reloading shenanigans
// redux throws errors if you don't split this into its own function
// :shrug:
function start() {
  const App = require('./app').default;
  ReactDOM.render(
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <App />
      </Provider>
    </ThemeProvider>,
    document.getElementById('app')
  );
}

start();

if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept('./app', start);
}
