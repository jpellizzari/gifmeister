import { lighten } from 'polished';

const black = '#1a1a1a';
const white = '#ffffff';
const grays = (() => {
  const g = {};

  for (let i = 1; i < 5; i += 1) {
    g[`gray${i}`] = lighten(i * 2 / 10, black);
  }
  return g;
})();

const colors = {
  primary: '#7D04FB', // purple
  black,
  white,
  background: white,
  // Grays drived from black. Use `gray1` -> `gray4`
  ...grays,
};

const theme = {
  colors,
  mixins: {
    disable: () => `
      cursor: 'not-allowed';
      background-color: ${colors.gray4};
      color: graytext;
    `,
  },
};

export default theme;
