import { chain, setIn, unsetIn } from 'icepick';

import { ActionTypes } from './actions';

export const initialState = {
  gifs: [],
  pagination: { offset: 0, limit: 15 },
  // Keep favorites in an object for faster lookups
  favorites: {},
};

export default function reducer(state, action) {
  switch (action.type) {
    case ActionTypes.RECEIVE_GIFS: {
      return chain(state)
        .set('gifs', action.gifs)
        .set('pagination', action.pagination)
        .value();
    }

    case ActionTypes.TOGGLE_FAVORITE: {
      // If the value exists, unset it.
      const key = ['favorites', action.image.id];
      const prev = state.favorites[action.image.id];
      return prev ? unsetIn(state, key) : setIn(state, key, action.image);
    }

    default:
      return state;
  }
}
