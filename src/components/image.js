import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { get } from 'lodash';

import { toggleFavorite } from '../actions';

import favoriteIcon from '../img/favorite-icon.svg';

// Images come in all shapes and sizes.
// Make it easier to select which one to use.
const imagePath = ['images', 'fixed_width_downsampled'];

const ImageWrapper = styled.div`
  position: relative;
`;

const FavoriteIcon = styled.div`
  position: absolute;
  opacity: ${props => (props.visible ? 1 : 0)};
  transition: opacity 0.25s ease;
  left: 0;
  top: 0;
  z-index: 2;
  height: 100%;
  width: 100%;
  background-image: url(${favoriteIcon});
  background-repeat: no-repeat;
  background-position: bottom 4px right 4px;
  background-size: 24px;
`;

const Styled = component => styled(component)`
  background-color: ${props => props.theme.colors.primary};
  cursor: pointer;
  height: ${props => props.height};
  margin-bottom: 8px;
  width: ${props => props.width};

  img {
    max-height: 100%;
    max-width: 100%;
    object-fit: cover; d
  }
`;

class Image extends React.PureComponent {
  handleClick = () => {
    if (this.props.onClick) {
      this.props.onClick(this.props.image);
    }
  };

  render() {
    const { className, image, isFavorite } = this.props;
    const { height, width, webp, title } = get(image, imagePath);

    return (
      <ImageWrapper onClick={this.handleClick} className={className}>
        <img height={height} width={width} src={webp} alt={title} />
        <FavoriteIcon visible={isFavorite} />
      </ImageWrapper>
    );
  }
}

export default connect(null, { toggleFavorite })(Styled(Image));
