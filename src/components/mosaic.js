import React from 'react';
import styled from 'styled-components';

const Styled = component => styled(component)`
  column-count: 5;
  column-gap: 8px;
  line-height: 0;
  overflow: auto;

  /* iPad portrait */
  @media (max-width: 768px) {
    column-count: 4;
  }

  @media (max-width: 400px) {
    column-count: 3;
  }
`;

class Mosaic extends React.PureComponent {
  render() {
    const { className, children } = this.props;
    return <div className={className}>{children}</div>;
  }
}

export default Styled(Mosaic);
