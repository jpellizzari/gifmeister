import React from 'react';
import styled from 'styled-components';

const Styled = component => styled(component)`
  margin: 0 auto;
  padding: 8px 16px;
`;

class Page extends React.PureComponent {
  render() {
    const { className, children } = this.props;
    return <div className={className}>{children}</div>;
  }
}

export default Styled(Page);
