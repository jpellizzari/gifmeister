import should from 'should';
import reducer, { initialState } from '../reducer';

import { receiveGifs, toggleFavorite } from '../actions';

import { gif } from './__fixtures__/gif';

describe('reducer', () => {
  let next;

  beforeEach(() => {
    next = initialState;
  });
  it('receiveGifs', () => {
    const gifs = [];
    const pagination = { total: 10000, count: 15, offset: 0 };
    next = reducer(initialState, receiveGifs(gifs, pagination));

    next.should.containDeep({
      gifs,
      pagination,
    });
  });
  it('toggleFavorite', () => {
    const gifs = [gif];
    // Set the favorite to on
    next = reducer({ ...initialState, gifs }, toggleFavorite(gif));

    should(next.favorites[gif.id]).deepEqual(gif);

    // Simulate the user unselecting the gif
    next = reducer(next, toggleFavorite(gif.id));

    should(next.favorites[gif.id]).not.be.true();
  });
});
