export const gif = {
  type: 'gif',
  id: '3o752dB7BILOk0mQ5G',
  slug: 'richbrian-dat-tick-rich-brian-3o752dB7BILOk0mQ5G',
  url: 'https://giphy.com/gifs/richbrian-dat-tick-rich-brian-3o752dB7BILOk0mQ5G',
  images: {
    fixed_width: {
      url: 'https://media3.giphy.com/media/3o752dB7BILOk0mQ5G/200w.gif',
      width: '200',
      height: '81',
      size: '669994',
      mp4: 'https://media3.giphy.com/media/3o752dB7BILOk0mQ5G/200w.mp4',
      mp4_size: '65702',
      webp: 'https://media3.giphy.com/media/3o752dB7BILOk0mQ5G/200w.webp',
      webp_size: '190312',
    },
  },
  title: 'dat $tick GIF by Rich Brian',
};
