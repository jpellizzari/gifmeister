import { concat, uniqBy } from 'lodash';

import giphy from '../giphy';
import { receiveGifs } from './creators';
import { PAGE_SIZE } from '../constants';

export function getTrending() {
  return dispatch =>
    giphy.trending().then(
      ({ data, pagination }) => dispatch(receiveGifs(data, pagination)),
      (err) => {
        console.error(err);
      }
    );
}

export function loadNextTrendingPage(increment = PAGE_SIZE) {
  return (dispatch, getState) => {
    const { pagination: { offset }, gifs } = getState();
    return giphy.trending(offset + increment).then(({ data, pagination }) =>
      // For some reason, giphy returns duplicates from the trending api.
      // Filter out all non-unique gifs
      dispatch(receiveGifs(uniqBy(concat(gifs, data), 'id'), pagination))
    );
  };
}
