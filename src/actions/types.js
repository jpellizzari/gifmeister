import { zipObject } from 'lodash';

const types = ['RECEIVE_GIFS', 'TOGGLE_FAVORITE'];

// Ensure that keys and values are always the same
const ActionTypes = zipObject(types, types);

export default ActionTypes;
