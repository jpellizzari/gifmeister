import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import nock from 'nock';
import { concat, times } from 'lodash';

import { initialState } from '../../reducer';
import { ActionTypes, getTrending, loadNextTrendingPage } from '../index';
import { GIPHY_API_HOST, GIPHY_TRENDING_PATH } from '../../constants';

const mockStore = configureMockStore([thunk]);

const gif = {
  type: 'gif',
  id: '3o752dB7BILOk0mQ5G',
  slug: 'richbrian-dat-tick-rich-brian-3o752dB7BILOk0mQ5G',
  url: 'https://giphy.com/gifs/richbrian-dat-tick-rich-brian-3o752dB7BILOk0mQ5G',
  images: {
    fixed_width: {
      url: 'https://media3.giphy.com/media/3o752dB7BILOk0mQ5G/200w.gif',
      width: '200',
      height: '81',
      size: '669994',
      mp4: 'https://media3.giphy.com/media/3o752dB7BILOk0mQ5G/200w.mp4',
      mp4_size: '65702',
      webp: 'https://media3.giphy.com/media/3o752dB7BILOk0mQ5G/200w.webp',
      webp_size: '190312',
    },
  },
  title: 'dat $tick GIF by Rich Brian',
};

function makeGifArray() {
  return times(3, () => ({
    ...gif,
    id: Math.random()
      .toString(36)
      .slice(2),
  }));
}

describe('async actions', () => {
  let store;

  beforeEach(() => {
    store = mockStore(initialState);
  });
  afterEach(() => {
    nock.cleanAll();
  });
  it('receiveGifs', () => {
    const gifs = makeGifArray();

    const pagination = { total: 10000, count: 3, offset: 0 };
    const desired = [
      {
        type: ActionTypes.RECEIVE_GIFS,
        gifs,
        pagination,
      },
    ];

    nock(`${GIPHY_API_HOST}:443`)
      .get(GIPHY_TRENDING_PATH)
      .query(true)
      .reply(200, { data: gifs, pagination });

    return store.dispatch(getTrending()).then(() => {
      store.getActions().should.deepEqual(desired);
    });
  });
  it('loadNextTrendingPage', () => {
    const gifs = makeGifArray();
    const gifs2 = makeGifArray();
    const pagination = { total: 10000, count: 3, offset: 3 };
    const desired = [
      {
        type: ActionTypes.RECEIVE_GIFS,
        gifs: concat(gifs, gifs2),
        pagination,
      },
    ];

    nock(`${GIPHY_API_HOST}:443`)
      .get(GIPHY_TRENDING_PATH)
      .query(true)
      .reply(200, { data: gifs2, pagination });

    // Simulate receiving the first set of gifs
    store = mockStore({ ...initialState, gifs, pagination });
    // store.dispatch(receiveGifs(gifs, { ...pagination, offset: 0 }));

    return store.dispatch(loadNextTrendingPage()).then(() => {
      store.getActions().should.deepEqual(desired);
    });
  });
});
