import ActionTypes from './types';

export function receiveGifs(gifs, pagination) {
  return {
    type: ActionTypes.RECEIVE_GIFS,
    gifs,
    pagination,
  };
}

export function toggleFavorite(image) {
  return {
    type: ActionTypes.TOGGLE_FAVORITE,
    image,
  };
}
