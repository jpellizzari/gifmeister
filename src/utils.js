export function createStorageEngine(namespace) {
  return {
    get(key) {
      return JSON.parse(localStorage.getItem(`${namespace}__${key}`));
    },
    set(key, value) {
      localStorage.setItem(`${namespace}__${key}`, JSON.stringify(value));
    },
  };
}
