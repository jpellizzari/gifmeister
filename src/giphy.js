import request from 'superagent';
import { GIPHY_API_HOST, GIPHY_TRENDING_PATH, API_TOKEN } from './constants';

const giphy = {
  trending: (offset = 0) =>
    new Promise((accept, reject) => {
      request
        .get(GIPHY_API_HOST + GIPHY_TRENDING_PATH)
        .query({ api_key: API_TOKEN, limit: 15, offset })
        .end((err, res) => {
          if (err) {
            return reject(err);
          }

          return accept(res.body);
        });
    }),
};

export default giphy;
